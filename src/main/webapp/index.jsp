<%-- 
    Document   : index
    Created on : 15-10-2021, 19:40:44
    Author     : gveja
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    </head>
    <body>
        <h1>Registrar candidato</h1>
        <form action="CandidatoController" method="POST">
            <div class="form-group">
                <label for="rut">Rut</label>
                <input type="text" class="form-control" id="rut" name="rut" placeholder="Ingresa rut">
            </div>
            <div class="form-group">
                <label for="nombre">Nombre</label>
                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Ingresa nombre">
            </div>
            <div class="form-group">
                <label for="apellido">Apellido</label>
                <input type="text" class="form-control" id="apellido" name="apellido" placeholder="Ingresa apellido">
            </div>
            <div class="form-group">
                <label for="partido">Partido</label>
                <input type="text" class="form-control" id="partido" name="partido" placeholder="Ingresa partido">
            </div>
        </div>
        <div class="form-group">
            <label for="partido">Fecha de nacimiento</label>
            <input type="text" class="form-control" id="fnac" name="fnac" placeholder="Ingresa fecha de nacimiento">
        </div>
        <div><button type="submit" name="accion" value="crear" class="btn btn-default">Crear</button></div>

    </form>
</body>
</html>
