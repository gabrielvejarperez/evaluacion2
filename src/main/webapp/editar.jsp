<%-- 
    Document   : editar
    Created on : 17-10-2021, 0:55:01
    Author     : gveja
--%>

<%@page import="root.evaluacionun2.entity.Candidato"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    Candidato candidato = (Candidato) request.getAttribute("candidato");

%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    </head>
    <body>
        <h1>Editar candidato</h1>
        <form action="CandidatoController" method="POST">
            <div class="form-group">
                <label for="rut">Rut</label>
                <input type="text" class="form-control" id="rut" name="rut" placeholder="Ingresa rut" value="<%= candidato.getRut()%>">
            </div>
            <div class="form-group">
                <label for="nombre">Nombre</label>
                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Ingresa nombre" value="<%= candidato.getNombre()%>">
            </div>
            <div class="form-group">
                <label for="apellido">Apellido</label>
                <input type="text" class="form-control" id="apellido" name="apellido" placeholder="Ingresa apellido" value="<%= candidato.getApellido()%>">
            </div>
            <div class="form-group">
                <label for="partido">Partido</label>
                <input type="text" class="form-control" id="partido" name="partido" placeholder="Ingresa partido" value="<%= candidato.getPartido()%>">
            </div>
        </div>
        <div class="form-group">
            <label for="partido">Fecha de nacimiento</label>
            <input type="text" class="form-control" id="fnac" name="fnac" placeholder="Ingresa fecha de nacimiento" value="<%= candidato.getFechaNacimiento()%>">
        </div>
        <div><button type="submit" class="btn btn-default" name="accion" value="editar">Modificar</button></div>

    </form>
</body>
</html>
