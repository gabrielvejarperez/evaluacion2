<%-- 
    Document   : listar
    Created on : 15-10-2021, 21:41:23
    Author     : gveja
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="root.evaluacionun2.entity.Candidato"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    List<Candidato> lista = (List<Candidato>) request.getAttribute("lista");
    Iterator<Candidato> itLista = lista.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    </head>
    <body>
        <form  name="form" action="CandidatoController" method="GET">
            <h1>Lista de candidatos</h1>

            <table border="1">
                <thead>
                    <th>Rut</th>
                    <th>nombre </th>
                    <th>apellido </th>
                    <th>Partido </th>
                    <th>Fecha de nacimiento</th>
                    <th> </th>
                </thead>
                <tbody>

                    <% while (itLista.hasNext()) {
                            Candidato candidato = itLista.next();%>
                    <tr>
                        <td><%=candidato.getRut()%></td>
                        <td><%=candidato.getNombre()%></td>
                        <td><%=candidato.getApellido()%></td>
                        <td><%=candidato.getPartido()%></td>
                        <td><%=candidato.getFechaNacimiento()%></td>
                        <td>
                            <input type="radio" name="seleccion" value="<%= candidato.getRut()%>">
                        </td>
                    </tr>
                    <%}%>
                </tbody>
            </table>
            <button type="submit" name="accion" value="crear" >Crear Candidato</button>
            <button type="submit" name="accion" value="ver" >Editar Candidato</button>
            <button type="submit" name="accion" value="eliminar" >Eliminar Candidato</button>  
        </form>
    </body>
</html>
